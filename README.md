
# File Structure :
1) config.php  : Configuration File 

2) todo.sql : Database File.

3) class/Main.class.php : Main class file which contains many usefull methods for database operations, mail sending, validation.

4) rest/.htaccess : HTACCESS file for the URL redirection

5) rest/Rest.inc.php : This class file contains REST Standard basis api related methods.

# Run the project
1.) rename folder name to "todo"
2.) import todo.sql
3.) change configuration setting depends on your setup..
4.) run http://localhost/todo/

#Bitbucket Repository
1.) https://bitbucket.org/amr1011/todo-api

# Requirements : 
1) PHP Version : 3.0 and above


