<?php
@include("../config.php");
class Main {
	
	/* BEGIN __CONSTRUNCT FUNCITON FOR THE MAIN CLASS */
		public function __construct(){
			/*BEGIN SETTING PAGE PER RECORD */
				if(isset($_GET['record']) && is_numeric($_GET['record'])){
					$_SESSION['pagerecords_limit']=$_GET['record'];
				}
			/* END SETTING PAGE PER RECORD */		
			$this->pagefilename = strtolower(basename($_SERVER['PHP_SELF']));

			$info_array = array();
			//$this->sitedata = $this->GetSingleRecord("site_settings",$info_array);

			/*define("SITE_TITLE",stripslashes($this->sitedata['site_title']));	
			define("SITE_EMAIL",stripslashes($this->sitedata['site_email']));*/	
			
		}
	/* END __CONSTRUNCT FUNCITON FOR THE MAIN CLASS */	

	/*BEGIN DATABASE CONNECTION FUNCTION WITH MYSQLI*/
		private function DBConnection(){
			@include("../config.php");

			$con = mysqli_connect($db_hostname,$db_username,$db_password,$db_name);

			if (mysqli_connect_errno()){
		  		echo "Failed to connect to MySQL: " . mysqli_connect_error();exit;
		  	} else {
		  		return $con;
		  	}
		}
	/*BEGIN DATABASE CONNECTION FUNCTION WITH MYSQLI*/



		/*BEGIN INSERT RECORD FUNCTION */
			public function InsertRecord($tablename, array $values){
				/*
					REQUIREMENT : 
						$tablename : Table Name where data will be inserted
						$values :  array 
									field_1 => value_1,field_2 => value_2,field_3 => value_3,field_n => value_n

									fields name and values which will be added for the record

					RETURN : LAST INSERTED IDs
				*/

				$last_inserted_id=0;	

				if(TABLE_PREFIX!=""){
					$tablename = TABLE_PREFIX.$tablename;
				}

				if(!empty($values)){

					$con = $this->DBConnection();

					$query_string = "insert into ".$tablename." set ";

					foreach($values as $key=>$value)
					{
						$query_string.=$key." = '".addslashes(mysqli_real_escape_string($con,$value))."' , ";	
					}

					$query_string = trim($query_string," , ");

					/* TO CHECK QUERY REMOVE ENABLE BELOW CODE */
					//echo $query_string;exit;
					
					mysqli_query($con,$query_string);
					$last_inserted_id = mysqli_insert_id($con);
					mysqli_close($con);
				}

				return $last_inserted_id;
			}
		/*END INSERT RECORD FUNCTION : InsertRecord()*/

			public function GetRecord($tablename,array $array){
				 
				/*
					REQUIREMENT : 
						$tablename : Table Name where data will be inserted
						$array :  array 
								fields : * or field_1, field_2, field_n #BY DEFAULT *	
								where : where condition as per your requirement 
								orderby	: order by parameter : #BY DEFAULT PRIMARY KEY
								ordertype	: order type parameter : #BY DEFAULT PRIMARY KEY desc
								limit : limit of the record, 10 or 20 or n...
								startfrom : record starts from
								groupby : group by
					RETURN : RECORD ARRAY
				*/

				if(TABLE_PREFIX!=""){
					$tablename = TABLE_PREFIX.$tablename;
				}

				$record = array();

				if(!isset($array['fields']) || $array['fields']=="") {$array['fields']="*";}

				$query_string = "select ".$array['fields']." from ".$tablename .' ' . "where 1=1 "; 	

				if(@$array['where']!=""){
					$query_string.=" and ".$array['where']." ";
				}

				//setting group by
				if(@$array['groupby']!=""){
					$query_string.=" group by ".$array['groupby'];
				}

				//seeting order by
				if(@$array['orderby']==""){
					$array['orderby']=1;
				}

				//setting order type
				if(@$array['ordertype']==""){
					$array['ordertype']="desc";
				}

				$query_string.=" order by ".$array['orderby']." ".$array['ordertype'];				

				//setting record start limit
				if(@$array['startfrom']==""){
					$array['startfrom']=0;
				}

				//setting record limit
				if(@$array['limit']>0 && is_numeric(@$array['limit'])){
					$query_string.=" limit ".$array['startfrom'].", ".$array['limit'];
				}

				/* TO CHECK QUERY REMOVE ENABLE BELOW CODE */
				//echo $query_string;exit;

				$con = $this->DBConnection();
				$query = mysqli_query($con,$query_string);
				if(@mysqli_num_rows($query)>0){

					while($data=mysqli_fetch_assoc($query)){
						$record[] = $data;
					}
					mysqli_free_result($query);

				}

				mysqli_close($con); 
				return $record;
			}
		/*END GET RECORD FUNCTION GetRecord()*/	

		

		/*BEGIN UPDATE RECORD FUNCTION */
			public function UpdateRecord($tablename,array $values,$where="")
			{

				/*
					REQUIREMENT : 
						$tablename : Table Name where data will be inserted
						$values :  	array 
									field_1 => value_1,field_2 => value_2,field_3 => value_3,field_n => value_n

									field names and values which will be updated.
						$where : 	Where condition in string : id = 1, id =1 and status=1

					RETURN : number of updated records 
				*/

				if(TABLE_PREFIX!=""){
					$tablename = TABLE_PREFIX.$tablename;
				}	

				if(!empty($values)){

					$con = $this->DBConnection();
					$query_string = "update ".$tablename." set ";

					foreach($values as $key=>$value){
						$query_string.=$key." = '".addslashes(mysqli_real_escape_string($con,$value))."' , ";
					}
					
					$query_string = trim($query_string," , ");

					if($where!=""){
						$query_string.=" where ".$where;
					}

					/* TO CHECK QUERY REMOVE ENABLE BELOW CODE */
					//echo $query_string;exit;
					
					mysqli_query($con,$query_string);
					$totalupdated = mysqli_affected_rows($con);
					mysqli_close($con);
					
				}

				//return mysql_affected_rows();
				return $totalupdated;
			}
		/*END UPDATE RECORD FUNCTION : UpdateRecord()*/	

		/*BEGIN DELETE RECORD FUNCTION*/	
			public function DeleteRecord($tablename, $where, $limit=0){

				/*
					REQUIREMENT : 
						$tablename : Table Name where data will be inserted
						$where : 	Where condition in string : id = 1, id =1 and status=1

					RETURN : number of deleted records 
				*/

				if(TABLE_PREFIX!=""){
					$tablename = TABLE_PREFIX.$tablename;
				}	

				$query_string = "delete from ".$tablename." ";

				if($where!=""){
					$query_string.=" where ".$where;
				}

				if($limit>0){
					$query_string.=" limit ".$limit;	
				}

				/* TO CHECK QUERY REMOVE ENABLE BELOW CODE */
				 //echo $query_string;exit;

				$con = $this->DBConnection();
				mysqli_query($con,$query_string);
				$totaldeleted = mysqli_affected_rows($con);
				mysqli_close($con);

				return $totaldeleted;
			}
		/*END DELETE RECORD FUNCTION : DeleteRecord() */		



}

?>
