<?php
	require_once("Rest.inc.php");
	
	class API extends REST {
		public $data = "";
		public function __construct(){
			parent::__construct();				// Init parent contructor
		}

		
		
		/*Add Todo List */
		public function AddTodo(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$info_array = array(
							'name'=>$_REQUEST['name']
						);

					//$this->response($this->json($info_array), 200);		
					//$todo_id = $this->InsertRecord("todo",$info_array);
					


					if(!(empty($_REQUEST['name']))) {
						$response_array['status']='success';
						$response_array['message']='Register successfully.';
						//$response_array['data']=array('user_id'=>$todo_id);
						$this->InsertRecord("todo",$info_array);
						$this->response($this->json($response_array), 200);


					} 
					else{
						$response_array['status']='fail';
						$response_array['message']='Insufficient data.';
						$response_array['data']='';
						$this->response($this->json($response_array), 200);
					}
					
		}
		
	
		/*Fetch Todo List */
		public function GetTodo(){
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}

				$info_array = array(
						"fields"=>"id,name"
					);
			$user_data = $this->GetRecord("todo",$info_array);
		
				$response_array['status']='success';
				$response_array['message']='Total '.count($user_data).' record(s) found.';
				$response_array['total_record']= count($user_data);
				$response_array['data']=$user_data;
			  	$this->response($this->json($response_array), 200);
			
		}

		

		public function delete(){

			
			$where ="id = ". $_REQUEST['id'];
			$this->DeleteRecord("todo",$where);
		}

		public function UpdateTodo(){

			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$info_array = array(
							'name'=>$_REQUEST['name']
							
						);
			
			$where = 'id ='.$_REQUEST['id'];

					
					$user_id = $this->UpdateRecord("todo",$info_array,$where);

					if($user_id == 0) {
						$response_array['status']='success';
						$response_array['message']='register successfully.';
						$response_array['data']=array('user_id'=>$user_id);
						$this->response($this->json($response_array), 200);
					} else {
						$response_array['status']='fail';
						$response_array['message']='insufficient data.';
						$response_array['data']='';
						$this->response($this->json($response_array), 204);
					}

		}

								
		
		/* Get contact phone numbers */						
		public function GetContactPhones(){	
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}

			$info_array = array(
						"fields"=>"contact_id,firstname,lastname,number",
						 "join" =>"LEFT JOIN gr_contact_phones ON gr_entity_contacts.contact_id = gr_contact_phones.id"
					);
			$user_data = $this->GetRecord("entity_contacts",$info_array);

			if(count($user_data)>0) {
				$response_array['status']='success';
				$response_array['message']='Total '.count($user_data).' record(s) found.';
				$response_array['total_record']= count($user_data);
				$response_array['data']=$user_data;
				$this->response($this->json($response_array), 200);
			} else {
				$response_array['status']='fail';
				$response_array['message']='Record not found.';
				$response_array['data']='';
				$this->response($this->json($response_array), 204);
			}
		}

		/* Get contact email */						
		public function GetContactEmails(){	
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}

			$info_array = array(
						"fields"=>"contact_id,firstname,lastname,email",
						 "join" =>"LEFT JOIN gr_contact_emails ON gr_entity_contacts.contact_id = gr_contact_emails.id "
					);
			$user_data = $this->GetRecord("entity_contacts",$info_array);

			if(count($user_data)>0) {
				$response_array['status']='success';
				$response_array['message']='Total '.count($user_data).' record(s) found.';
				$response_array['total_record']= count($user_data);
				$response_array['data']=$user_data;
				$this->response($this->json($response_array), 200);
			} else {
				$response_array['status']='fail';
				$response_array['message']='Record not found.';
				$response_array['data']='';
				$this->response($this->json($response_array), 204);
			}
		}

		/* Get contact email */						
		public function GetContactSMS(){	
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}

			$info_array = array(
						"fields"=>"contact_id,firstname,lastname,number",
						 "join" =>"LEFT JOIN gr_contact_sms ON gr_entity_contacts.contact_id = gr_contact_sms.id "
					);
			$user_data = $this->GetRecord("entity_contacts",$info_array);

			if(count($user_data)>0) {
				$response_array['status']='success';
				$response_array['message']='Total '.count($user_data).' record(s) found.';
				$response_array['total_record']= count($user_data);
				$response_array['data']=$user_data;
				$this->response($this->json($response_array), 200);
			} else {
				$response_array['status']='fail';
				$response_array['message']='Record not found.';
				$response_array['data']='';
				$this->response($this->json($response_array), 204);
			}
		}

		/* Get contact entities */						
		public function GetContactEntities(){	
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}

			$info_array = array(
						"fields"=>"gr_entities.entity_id,entity_name,entity_dba",
						 "join" =>" LEFT JOIN gr_entities ON gr_contacts_to_entities.entity_id = gr_entities.entity_id"
					);
			$user_data = $this->GetRecord("contacts_to_entities",$info_array);

			if(count($user_data)>0) {
				$response_array['status']='success';
				$response_array['message']='Total '.count($user_data).' record(s) found.';
				$response_array['total_record']= count($user_data);
				$response_array['data']=$user_data;
				$this->response($this->json($response_array), 200);
			} else {
				$response_array['status']='fail';
				$response_array['message']='Record not found.';
				$response_array['data']='';
				$this->response($this->json($response_array), 204);
			}
		}

		/*Add contact phone */
		public function AddContactPhone(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$type = $_REQUEST['type'];
			$number =$_REQUEST['number'];

			if($type == '' || $number == ''){

				$response_array['status']='fail';
				$response_array['message']='insufficient data.';
				$response_array['data']='';
				 $this->response($this->json($response_array), 204);
				//echo json_encode($response_array,204);
			}
			else{

				$info_array = array(
							'type'=>$type,
							'number'=>$number
						);

				$user_id = $this->InsertRecord("contact_phones",$info_array);

				$response_array['status']='success';
				$response_array['message']='register successfully.';
				$response_array['data']=array('user_id'=>$user_id);
				$this->response($this->json($response_array), 200);

				
			}
		}

		/*Add contact email */
		public function AddContactEmail(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$email = $_REQUEST['email'];
			$sms_status =$_REQUEST['sms_status'];

			if($email == '' || $sms_status == ''){

				$response_array['status']='fail';
				$response_array['message']='insufficient data.';
				$response_array['data']='';
				 $this->response($this->json($response_array), 204);
				//echo json_encode($response_array,204);
			}
			else{

				$info_array = array(
							'email'=>$email,
							'sms_status'=>$sms_status
						);

				$user_id = $this->InsertRecord("contact_emails",$info_array);

				$response_array['status']='success';
				$response_array['message']='register successfully.';
				$response_array['data']=array('user_id'=>$user_id);
				$this->response($this->json($response_array), 200);
				
			}
		}

		/*Add contact SMS*/
		public function AddContactSMS(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$number = $_REQUEST['number'];
			$email_status =$_REQUEST['email_status'];

			if($number == '' || $email_status == ''){

				$response_array['status']='fail';
				$response_array['message']='insufficient data.';
				$response_array['data']='';
				 $this->response($this->json($response_array), 204);
				
			}
			else{

				$info_array = array(
							'number'=>$number,
							'email_status'=>$email_status
						);

				$user_id = $this->InsertRecord("contact_sms",$info_array);

				$response_array['status']='success';
				$response_array['message']='register successfully.';
				$response_array['data']=array('user_id'=>$user_id);
				$this->response($this->json($response_array), 200);
				
			}
		}

		
	}
	// Initiiate Library
	$api = new API();
	$api->processApi();
?>

