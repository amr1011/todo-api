var CVCrud = (function() {


	/* Bind Events */
	function bindEvents(){
	



	$('body').off('click','.add-todo').on('click', '.add-todo', function(){ 
			addTodo();
	}); 

	$('body').off('click','.delete').on('click', '.delete', function(){ 
		var todoId = $(this).attr("todo-id");
		deleteTodo(todoId);
	});

	$('body').off('click','.edit').on('click', '.edit', function(){ 
		$('#message').html(" ");
		 editName = $(this).parents('tr').find("td:eq(1)").text();
		 id = $(this).parents('tr').find("td:eq(0)").text();

  		 $('.todo-name').val(editName);
  		 $(".update-todo").attr('update-id' ,id);

  		$(".add-todo").addClass("hide");
  		$(".update-todo").removeClass("hide");

  	}); 

  	$('body').off('click','.fresh').on('click', '.refresh', function(){ 
		getTodo();
		$('#message').html(" ");
		$(".update-todo").addClass("hide");
		$(".add-todo").removeClass("hide");
	});

	$('body').off('click','.update-todo').on('click', '.update-todo', function(){ 
		var todoId = $(this).attr("update-id");
		update(todoId);
		$(".add-todo").removeClass("hide");
  		$(".update-todo").addClass("hide");
	});

	}


	/* End of Bind Events */

	/* PUBLIC METHODS */
	

	function getTodo(){
		

		$.ajax({
		     type: "GET",
		     url: BASE_URL +"/GetTodo",
		     success: function(oTodoList){
		     	
		     	displayTodo(oTodoList);
		     	//console.log(oTodoList);
		     	
		       }
			});
	}

	function addTodo(){
		
		var name = $('.todo-name').val();
			
			

		var postData = {'name' : name};

		$.ajax({
	     type: "POST",
	     url: BASE_URL +"AddTodo",
	     data: postData ,
	     success: function(msg){
	     	if(msg.status == "success"){
	     		
	     		getTodo();
	     		$('#message').html(" ");
	     		$('#message').append(successTemplate(msg.message));
	     		$('.todo-name').val(" ");
	     	}
	     	else{
	     		$('#message').html(" ");
	     		$('#message').append(errorTemplate(msg.message));

	     	}
	     	
	     	}
	     	
		});
	

	}

	function successTemplate(message){
		var sHtml = '<div class="alert alert-success">'
            sHtml +=  '<strong>'+message+'</strong>'
      	    sHtml +=  '</div>'

      	    return $(sHtml);
	}

	function errorTemplate(message){
		var sHtml = '<div class="alert alert-danger">'
            sHtml +=  '<strong>'+message+'</strong>'
      	    sHtml +=  '</div>'

      	    return $(sHtml);
	}

	
	
	function todoListemplate() {

            sHtml = '<tr>';
	            sHtml +=   '<td class="id">1</td>';
	            sHtml +=   '<td class="name">xx</td>';
	            sHtml +=   '<td><input type="button" class="btn btn-info edit" value="Edit">&nbsp'
	            sHtml +=  '<input type="button" class="btn btn-danger delete" value="Delete">'
	            sHtml +=   '</td>';
            sHtml += '</tr>';

            return $(sHtml);
	}

	function displayTodo(oTodoList){
		var uiTodoContainer = $("#showlist");

		uiTodoContainer.html("");
		
		list = oTodoList.data;
	

		for(i in list){
			
			var id = list[i].id,
				name = list[i].name,
				
				uiTemplate = todoListemplate();
				
			
			uiTemplate.find(".id").html(id);
			uiTemplate.find(".name").html(name);
			uiTemplate.find(".delete").attr('todo-id' ,id);
			uiTodoContainer.append(uiTemplate);

			
		}
	}
	function deleteTodo(todoId){
		var message ="Task Deleted!";
		var postData = {'id':todoId};
			$.ajax({
		     type: "POST",
		     url: BASE_URL +"delete",
		     data: postData ,
		     success: function(msg){
						alert("Are you sure you want to delete?")
						getTodo();
						$('#message').html(" ");
	     				$('#message').append(errorTemplate(message));
		         }
			});
	}

	function update(todoId){

		var name = $('.todo-name').val(),
			message = "Data updated!"

		var postData = {'name':name,'id':todoId};
			$.ajax({
		     type: "POST",
		     url: BASE_URL +"UpdateTodo",
		     data: postData ,
		     success: function(msg){
		     		getTodo();
	     		$('#message').html(" ");
	     		$('#message').append(successTemplate(message));
	     		$('.todo-name').val(" ");

					
		         }

			});

	}
	

	return {
		displayTodo:displayTodo,
		bindEvents : bindEvents,
		getTodo:getTodo
	
	}

})();

$(document).ready(function() {
	
	CVCrud.getTodo();
	CVCrud.bindEvents();
	
	/*CVProjects.getFilterBySection();
	
	if(window.location.href.indexOf("stages") > -1){

		CVProjects.bindEvents();
	}*/

		

});
