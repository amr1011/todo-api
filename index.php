<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script type="text/javascript">
    var BASE_URL = "http://localhost/todo/api/rest/"
  </script>


  
  <title>Todo List App</title>
</head>
<body>
<div class="container">
  <div class="col-md-offset-2 col-md-8">
    <div class="row">
      <h1>Todo List</h1>
    </div>

     <div id="message">

     </div>
    <!--   <div class="alert alert-success">
        <strong>Success:</strong> {{ Session::get('success') }}
      </div>

      <div class="alert alert-danger">
        <strong>Error:</strong>
        <ul>
          
            <li></li>
         
        </ul>
      </div> -->
  
    <div class="row" style='margin-top: 10px; margin-bottom: 10px;'>
     <!--  <form action="" method='POST'> -->
   
        <div class="col-md-7">
          <input type="text" name='newTaskName' class='form-control todo-name'>
        </div>

        <div class="col-md-5">
        <button class="btn btn-success refresh"><span class="glyphicon glyphicon-refresh"></span> Refresh</button>
          <input type="button"  class='btn btn-primary  add-todo' value='Add Task'>
          <input type="button"  class='btn btn-warning update-todo hide' value='Update Task'>
        </div>
      <!-- </form> -->
    </div>

  
      <table class="table">
        <thead>
          <th>Task #</th>
          <th>Name</th>
          <th>Action</th>
         
        </thead>

        <tbody id="showlist">
      
            <!-- <tr>
              <th>1</th>
              <td>xx</td>
              <td><a href="" class='btn btn-warning'>Edit</a>
             <a href="" class='btn btn-danger'>Delete</a>
           
              </td>
            </tr> -->
        
        </tbody>
      </table>
 

    <div class="row text-center">
   
    </div>

  </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="crud.js"></script>


</body>
</html>